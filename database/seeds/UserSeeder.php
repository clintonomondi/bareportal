<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname' => 'Clinton Tester',
            'lname' => 'Tester',
            'email' => 'admin@gmail.com',
            'phone' => '0712083128',
            'role' => 'admin',
            'password' => bcrypt('admin'),
        ]);
    }
}
