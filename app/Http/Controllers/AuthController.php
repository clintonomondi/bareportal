<?php

namespace App\Http\Controllers;

use App\Otp;
use App\Otp_Logs;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Knox\AFT\Facades\AFT;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))

        return ['status'=>false,'message' => 'Invalid password or email'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return ['status'=>true,'user'=>$user,
        'access_token' => $tokenResult->accessToken,
        'token_type' => 'Bearer',
        'expires_at' => Carbon::parse(
            $tokenResult->token->expires_at
        )->toDateTimeString()
    ];
    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        return response()->json([
        'message' => 'Successfully logged out'
    ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public  function getotp(Request $request){
        $phone=User::where('phone',$request->phone)->first();
        if(empty($phone)){
            return ['status'=>false,'message'=>'The phone number is  not in our records'];
        }
        $code=mt_rand(100000, 999999);
        $request['code']=$code;
        $request['user_id']=$phone->id;
        Otp::create($request->all());
        AFT::sendMessage($request->phone, 'Your six digit password verification code is: '.$code.'  The code will expire after 60 minutes. @BareBalqesaAdvocates');
        return ['status'=>true,'message'=>'A six digit verification code has been sent,Please enter the code','phone'=>$request->phone,'code'=>$code,'user'=>$phone];

    }

    public  function verify(Request $request){
        $otp=Otp::where('phone',$request->phone)->where('status','unused')->where('code',$request->code)->where('created_at', '>=', Carbon::now()->subMinutes(60)->toDateTimeString())->first();
        if (empty($otp)){
            return ['status'=>false,'message'=>'Invalid code, please try again'];
        }
        return ['status'=>true,'message'=>'Please enter new password','code'=>$request->code];
    }


    public  function setPassword(Request $request){
        if (empty($request->password)){
            return ['status'=>false,'message'=>'Enter phone number'];
        }

        $otp=Otp::where('phone',$request->phone)->where('status','unused')->where('code',$request->code)->where('created_at', '>=', \Carbon\Carbon::now()->subMinutes(60)->toDateTimeString())->first();

        if (empty($otp)){
            return ['status'=>false,'message'=>'Invalid code, please try again'];
        }

        if ($request->input('password') !== $request->input('repass')) {
            return ['status'=>false,'message'=>'Password are not matching'];
        }
        $id =User::where('phone',$request->phone)->first();
        $currentUser = User::findOrFail($id->id);
        $currentUser->password = Hash::make($request->input('password'));
        $currentUser->save();

        Otp::where('phone', $request->phone)->update(['status' => 'used']);
        return ['status'=>true,'message'=>'Password set successfully,please login with this password now.'];

    }



}
