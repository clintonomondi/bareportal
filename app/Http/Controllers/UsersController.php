<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public  function users(){
        $users=User::all();
        return $users;
    }

    public  function adduser(Request $request){
        if(empty($request->avatar)){
            return ['status'=>false,'message'=>'You must select a photo'];
        }
        $request->validate([
            'email' => 'required|string',
            'phone' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        $check=User::where('email',$request->email)->first();
        if(!empty($check)){
            return ['status'=>false,'message'=>'Email already in use'];
        }

        $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatar')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);


        $request['pic']=$fileNameToStore;
        $data=User::create($request->all());
        return ['status'=>true,'message'=>'Staff added successfully'];
    }


    public  function getUser($id){
        $user=User::find($id);
        return $user;
    }

    public  function edituser(Request $request,$id){
        $request->validate([
            'email' => 'required|string',
            'phone' => 'required',
            'fname' => 'required',
            'lname' => 'required',
        ]);
        if($request->imagechnage=='yes'){
            $request->validate([
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            ]);

            $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
            $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extesion=$request->file('avatar')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore=$filename.'_'.time().'.'.$extesion;
            //uploadimage
            $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);
            $request['pic']=$fileNameToStore;
        }

        $user=User::find($id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Staff updated successfully successfully'];
    }
}
