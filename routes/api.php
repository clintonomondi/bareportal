<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('getotp', 'AuthController@getotp');
    Route::post('verify', 'AuthController@verify');
    Route::post('setPassword', 'AuthController@setPassword');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        //users
        Route::get('/getUsers', 'UsersController@users');
        Route::post('/adduser', 'UsersController@adduser');
        Route::get('/getUser/{id}', 'UsersController@getUser');
        Route::post('/edituser/{id}', 'UsersController@edituser');
    });
});
