import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueSimpleAlert from "vue-simple-alert";

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueSimpleAlert);


import App from './view/App'
import Home from './view/Home'
import Login from './view/auth/Login'
import Users from './view/users/Users'
import Addusers from './view/users/adduser'
import Editusers from './view/users/edituser'
import getOtp from './view/auth/getOTP'
import Verify from './view/auth/verify'
import Password from './view/auth/setpassword'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/users',
            name: 'users',
            component: Users
        },
        {
            path: '/users/add',
            name: 'addusers',
            component: Addusers
        },
        {
            path: '/users/add/:id',
            name: 'editusers',
            component: Editusers
        },
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/getotp',
            name: 'getotp',
            component: getOtp,
            meta: { hideNavigation: true }
        },
        {
            path: '/verify',
            name: 'verify',
            component: Verify,
            meta: { hideNavigation: true }
        },
        {
            path: '/setpassword',
            name: 'setpassword',
            component: Password,
            meta: { hideNavigation: true }
        },


    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});

