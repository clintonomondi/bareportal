<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bare Portal</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('/asset/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/asset/vendors/base/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('/asset/css/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('/images/logo.jpeg')}}" />
</head>

<body>
<div id="app">
    <app></app>
</div>
<script src="{{ mix('/js/app.js') }}"></script>


<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('/asset/vendors/base/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{asset('/asset/vendors/chart.js/Chart.min.js')}}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('/asset/js/off-canvas.js')}}"></script>
<script src="{{asset('/asset/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('/asset/js/template.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('/asset/js/chart.js')}}"></script>
<script src="{{asset('/js/common.js')}}"></script>
<script src="{{asset('/asset/js/file-upload.js')}}"></script>
<!-- End custom js for this page-->
</body>

</html>
